var devicename; // the name of this screen and specified in the URL
var imageCount = 7; // the maximum number of images available
window.onbeforeunload = closingCode;
var sscreen = io.connect('http://localhost/sscreen')

document.addEventListener("DOMContentLoaded", function(event) {
    devicename = getQueryParams().name;
    if (devicename) {
        var text = document.querySelector('#name');
        text.textContent = devicename;
    }
    connectToServer();
});

function showImage (index){
    var img = document.querySelector('#image');
    var msg = document.querySelector('#msg');
    if (index >= 0 && index <= imageCount){
        img.setAttribute("src", "images/" +index +".jpg");
        msg.style.display = 'none';
        img.style.display = 'block';
    }
}

function clearImage(){
    var img = document.querySelector('#image');
    var msg = document.querySelector('#msg');
    img.style.display = 'none';
    msg.style.display = 'block';
}

function getQueryParams() {
    var qs =  window.location.search.split("+").join(" ");

    var params = {}, tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
}

function closingCode(){
    sscreen.disconnect();
}

function connectToServer(){
    // TODO connect to the socket.io server
    sscreen.emit('new screen', { name: devicename });
}

sscreen.on('replace imgScreen', function (data) {
    showImage (data["index"]);
});

//if a remote is closed, clear all screens
//can be expanded to only clear screens that are connected to 
// the remote by passing socketID of remote from index.js
sscreen.on('clear image', function() {
    clearImage();
});
           