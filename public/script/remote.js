var currentImage = 0; // the currently selected image
var imageCount = 7; // the maximum number of images available
window.onbeforeunload = closingCode;
var remote = io.connect('http://localhost/remote');
var currentScreens = {};

function showImage(index) {
    // Update selection on remote
    currentImage = index;
    var images = document.querySelectorAll("img");
    document.querySelector("img.selected").classList.toggle("selected");
    images[index].classList.toggle("selected");

    // Send the command to the screen
    // TODO
    //alert("TODO send the index to the screen")
    remote.emit('replace img', {
        "index": index
    });
    console.log("after emit");
}

function initialiseGallery() {
    var container = document.querySelector('#gallery');
    var i, img;
    for (i = 0; i < imageCount; i++) {
        img = document.createElement("img");
        img.src = "images/" + i + ".jpg";
        document.body.appendChild(img);
        var handler = (function (index) {
            return function () {
                showImage(index);
            }
        })(i);
        img.addEventListener("click", handler);
    }

    document.querySelector("img").classList.toggle('selected');
}

document.addEventListener("DOMContentLoaded", function (event) {
    initialiseGallery();

    document.querySelector('#toggleMenu').addEventListener("click", function (event) {
        var style = document.querySelector('#menu').style;
        style.display = style.display == "none" || style.display == "" ? "block" : "none";
    });
    connectToServer();
});

function connectToServer() {
    // TODO connect to the socket.io server
    remote.emit('new remote');
}

function closingCode() {
    remote.disconnect();
}

//add a screen to the list
remote.on('add screen', function (data) {
    //set the button id to the name of the screen
    connectButton = $('<button />').addClass('connectButton').text('Connect').attr('id', data);

    //add screen as a list element with button alongside
    $("#menu ul").append(
        $('<li>').append(data)
        .append(connectButton));
});

//remove a screen from the list
remote.on('remove screen', function (data) {
    //remove screen from list
    $('#menu li:contains(' + data + ')').remove();
});

//connect and disconnect the selected screen on button click
$(document).ready(function () {
    $("#menu").on("click", ".connectButton", function () {

        if ($(this).text() == 'Connect') {
            remote.emit('connect screen', this.id);
            $(this).text("Disconnect");
        } else {
            remote.emit('disconnect screen', this.id);
            $(this).text("Connect");
        }
    });
});