var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var currentScreens = {};
var currentRemotes = {};
var currentImage;

app.use(express.static('public'));

var sscreen = io
    .of('/sscreen')
    .on('connection', function (socket) {
        socket.on('new screen', function (data) {
            var newScreen = data["name"];

            //add new screen to list of screens
            currentScreens[socket.id] = {
                userID: newScreen,
                socketID: socket
            };

            //add new screen name to the list on remotes
            remote.emit('add screen', newScreen);
        });

        socket.on('disconnect', function () {
            //remove screen from remote list
            if (currentScreens[socket.id]) {
                console.log('removing: ' + currentScreens[socket.id].userID);
                remote.emit('remove screen', currentScreens[socket.id].userID);

                //remove screen from map of screens
                delete currentScreens[socket.id];
                console.log('user disconnected');
                //console.log(currentScreens);
            }
        });

    });

var remote = io
    .of('/remote')
    .on('connection', function (socket) {
        socket.on('new remote', function () {

            //send all screen names to the new remote
            for (var key in currentScreens) {
                socket.emit('add screen', currentScreens[key].userID);
                console.log('sending ' + currentScreens[key].userID + ' to remote ' + socket.id);
            }

            //add new remote to list of remotes
            currentRemotes[socket.id] = socket.id;
            //console.log(currentRemotes);
        });

        //Emit image to all screens connected to the room associated to the socket id of the remote
        socket.on('replace img', function (data) {
            currentImage = data;
            console.log("Emit to room " + socket.id + " image: " + data["index"]);
            sscreen.to(socket.id).emit('replace imgScreen', data);
        });

        //connect a particular screen on button click
        socket.on('connect screen', function (name) {

            //find the socket id of the screen and add it to the connected room
            for (var key in currentScreens) {
                if (currentScreens[key].userID == name) {
                    console.log('connect screen ' + name + ' to remote ' + socket.id);
                    
                    //Each remote has a separate room
                    //create a room where the name is the socket id of the remote. 
                    currentScreens[key].socketID.join(socket.id);
                    
                    //set image on connected screens
                    if (currentImage != null) {
                        currentScreens[key].socketID.emit('replace imgScreen', currentImage);
                    }
                }
            }

        });

        //disconnect a particular screen on button click
        socket.on('disconnect screen', function (name) {

            //find the socket with the given name from the map and remove it from the connected room
            for (var key in currentScreens) {
                if (currentScreens[key].userID == name) {
                    console.log('disconnect screen ' + name + ' from remote ' + socket.id);
                    currentScreens[key].socketID.leave(socket.id); //'connected screens room');
                    currentScreens[key].socketID.emit('clear image');
                }
            }
        });

        //print debug messages sent from remote
        socket.on('debug remote', function (msg) {
            console.log("remote debug: " + msg);
        });

        //clear screens when remote is closed
        socket.on('disconnect', function () {
            //remove screen from remote list
            if (currentRemotes[socket.id]) {
                console.log('removing remote: ' + currentRemotes[socket.id]);
                sscreen.emit('clear image');

                //remove remote from map of remotes
                delete currentRemotes[socket.id];
                //console.log('image cleared');
                //console.log(currentRemotes);
            }
        });
    });

http.listen(8080, function () {
    console.log('listening on *:8080');
});

function sendScreenNames() {
    //send all screen names to the remotes
    for (var key in currentScreens) {
        remote.emit('change screenlist', currentScreens[key].userID);
        //console.log('sending ' + currentScreens[key].userID);
    }

};